const path = require('path');
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
require("dotenv").config({ path: path.join(__dirname, ".env") });
const hbs = require('hbs');

var router = express.Router();

const response = require('./utils/response');
const routes = require('./routes');
const app = express();											

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'hbs');

app.use(express.static(path.join(__dirname,'public')));
app.use(response); 

app.use(session({
	secret: process.env.SESSION_SECRET_KEY,
	resave: false,
    saveUninitialized: false,
	cookie: { maxAge : 6044800 }
}));

app.use('/', routes);
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`))
