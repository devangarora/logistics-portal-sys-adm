DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
	`user_id` INT(11) NOT NULL AUTO_INCREMENT,
	`username` varchar(20) NOT NULL UNIQUE,
	`password` varchar(20) NOT NULL,
	`category` varchar(20) NOT NULL UNIQUE,
	`email` varchar(30) NOT NULL,
	`admin` BOOLEAN NOT NULL DEFAULT FALSE,
	PRIMARY KEY (`user_id`)
);

DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
	`item_id` INT(11) NOT NULL AUTO_INCREMENT,
	`item_name` varchar(30) NOT NULL,
	`quantity` INT(11) NOT NULL,
	`description` TEXT NOT NULL,
	PRIMARY KEY (`item_id`)
);

DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests` (
	`order_id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NOT NULL,
	`category` varchar(20) NOT NULL,
	`item_id` INT(11) NOT NULL,
	`item_name` varchar(30) NOT NULL,
	`reqd_quantity` INT(11) NOT NULL,
	`quantity` INT(11) NOT NULL,
	`description` VARCHAR(255) NOT NULL,
	`date_of_request` DATE NOT NULL,
	`status` varchar(20) DEFAULT 'pending',
	`fac_approved` varchar(20) DEFAULT 'pending',
	PRIMARY KEY (`order_id`)
);

DROP TABLE IF EXISTS `requested_items`;
CREATE TABLE `requested_items` (
	`req_id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NOT NULL,
	`category` varchar(20) NOT NULL,
	`item` varchar(30) NOT NULL,
	`quantity` INT(11) NOT NULL,
	`description` TEXT NOT NULL,
	`date_of_request` DATE NOT NULL,
	`status` varchar(20) DEFAULT 'pending',
	`fac_approved`varchar(20) DEFAULT 'pending',
	PRIMARY KEY (`req_id`)
);

ALTER TABLE `requests` ADD CONSTRAINT `requests_fk0` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`);

ALTER TABLE `requests` ADD CONSTRAINT `requests_fk1` FOREIGN KEY (`item_id`) REFERENCES `items`(`item_id`);

ALTER TABLE `requested_items` ADD CONSTRAINT `requested_items_fk0` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`);
