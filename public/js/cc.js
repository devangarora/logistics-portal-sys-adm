const addItemsButton = document.querySelector('.show-add-items');
const addItemForm = document.querySelector('.add-item');
const rButtonSelector = document.querySelector('.select');
const elmnt = document.querySelector(".itemName");
addItemForm.style.display = 'none';


addItemsButton.addEventListener('click', (e) => {
    if (addItemForm.style.display === 'none') {
        addItemForm.style.display = '';
        elmnt.scrollIntoView(true);
    } else {
        addItemForm.style.display = 'none';
    }
    e.preventDefault();
});

const itemNames = document.querySelectorAll(".item-name");
const searchBar = document.querySelector(".search-bar");
const items = document.querySelectorAll(".item-row");
console.log(items);
searchBar.addEventListener('keyup', (e) => {
    var filter = e.target.value.toUpperCase();
    console.log(filter);
    for(i = 0; i < itemNames.length; i++) {
        var a = itemNames[i].getElementsByTagName("a")[0];
        if(a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            items[i].style.display = "";
        } else {
            items[i].style.display = "none";
        }

    }
});

