const adminAddItem = document.querySelector('.ad-addItem');
const adminUpdateItem = document.querySelector('.ad-updateItem');
const adminAddItemForm = document.querySelector('.ad-addItemForm');
const adminUpdateItemForm = document.querySelector('.ad-updateItemForm');
const elmnt = document.querySelector(".itemName");
  
adminAddItemForm.style.display = 'none';
adminUpdateItemForm.style.display = 'none';

adminAddItem.addEventListener('click', (e) => {
    if (adminAddItemForm.style.display === 'none') {
        adminAddItemForm.style.display = '';
        adminUpdateItemForm.style.display = 'none';
        elmnt.scrollIntoView(true);
    } else {
        adminAddItemForm.style.display = 'none';
        adminUpdateItemForm.style.display = 'none';
        
    }
    e.preventDefault();
});

adminUpdateItem.addEventListener('click', (e) => {
    if (adminUpdateItemForm.style.display === 'none') {
        adminUpdateItemForm.style.display = '';
        adminAddItemForm.style.display = 'none';
        elmnt.scrollIntoView(true);
    } else {
        adminAddItemForm.style.display = 'none';
        
        adminUpdateItemForm.style.display = 'none';
    }
    e.preventDefault();
});

const itemNames = document.querySelectorAll(".item-name");
const searchBar = document.querySelector(".search-bar");
const items = document.querySelectorAll(".item-row");
console.log(items);
searchBar.addEventListener('keyup', (e) => {
    var filter = e.target.value.toUpperCase();
    console.log(filter);
    for(i = 0; i < itemNames.length; i++) {
        var a = itemNames[i].getElementsByTagName("a")[0];
        if(a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            items[i].style.display = "";
        } else {
            items[i].style.display = "none";
        }

    }
});
