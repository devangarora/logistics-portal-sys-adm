const addItems = document.querySelector('.show-add-items');
const addItemsForm = document.querySelector('.add-items-form');

addItemsForm.style.display = 'none';

addItems.addEventListener('click', (e) => {
    if (addItemsForm.style.display == 'none')
        addItemsForm.style.display = '';
    else
        addItemsForm.style.display = 'none'
    e.preventDefault();
})
