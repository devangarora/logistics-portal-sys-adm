const db = require('../config/conn');
const to = require('../utils/to');
const path = require('path');
require("dotenv").config({ path: path.join(__dirname, "../.env") });

let exp = {};

exp.displayPage = async (req, res) => {
    let err, request;
    [err, request] = await to(db.query(`SELECT * FROM requests WHERE status = "pending" AND fac_approved = "pending"`));

    let err1, newRequest;
    [err, newRequest] = await to(db.query(`SELECT * FROM requested_items WHERE status = "pending" AND fac_approved = "pending"`));
    
    if(err || err1) {
        res.sendError(null, 'Could not load faculty page');
    } else {
        res.render('faculty', { request, newRequest });
    }
}

exp.approve = async (req, res) => {
    
    if(req.body.approve) {
        let orderID = req.body.approve;
        let err, result;
        [err, result] = await to(db.query(`UPDATE requests SET fac_approved = "approved" WHERE order_id = ?`, [ orderID ]));
        if(err) {
            res.sendError(null, 'Could not approve');
        }
    } else if(req.body.newApprove) {
        let reqID = req.body.newApprove;
        let err, result;
        [err, result] = await to(db.query(`UPDATE requested_items SET fac_approved = "approved" WHERE req_id = ?`, [ reqID ]));
        if(err) {
            res.sendError(null, 'Could not approve');
        }
    } else {
        res.sendError(null, 'some error occured');
    }

    res.redirect('/faculty');
}

exp.disapprove = async (req, res) => {
    
    if(req.body.disapprove) {
        let orderID = req.body.disapprove;
        let err, result;
        [err, result] = await to(db.query(`UPDATE requests SET fac_approved = "disapproved", status = "disapproved by FA" WHERE order_id = ?`, [ orderID ]));
        if(err) {
            res.sendError(null, 'Could not approve');
        }
    } else if(req.body.newDisapprove) {
        let reqID = req.body.newDisapprove;
        let err, result;
        [err, result] = await to(db.query(`UPDATE requested_items SET fac_approved = "disapproved", status = "disapproved by FA" WHERE req_id = ?`, [ reqID ]));
        if(err) {
            res.sendError(null, 'Could not approve');
        }
    } else {
        res.sendError(null, 'some error occured');
    }

    res.redirect('/faculty');
}

module.exports = exp;