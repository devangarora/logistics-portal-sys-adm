const db = require('../config/conn');
const to = require('../utils/to');
const alert = require('alert-node');
const path = require('path');
require("dotenv").config({ path: path.join(__dirname, "../.env") });
const nodemailer = require('nodemailer')
const jwt = require('jwt-simple');

let exp = {};

exp.loadLogin = (req, res) => {
    res.render('index', {});
}

exp.forgotPasswordPage = (req, res) => {
    res.render('forgot-password');
}

exp.sendResetMail = async (req, res) => {
    
    if (req.body.email !== undefined) {
        let emailAddress = req.body.email;

        let err, result;
        [err, result] = await to(db.query(`SELECT * FROM users WHERE email = ?`, [ emailAddress ]));
        
        if(err) {
            console.log(err);
        } else {
            
            var payload = {
                id: result[0].user_id, // User ID from database
                email: emailAddress
            };

            var secret = result[0].password;
            var token = jwt.encode(payload, secret);

            var transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 587,
                secure: false,
                service:'gmail',
                auth:{
                    user: process.env.email_id,
                    pass: process.env.password
                }
            });

            let link = "http://localhost:5000/resetpassword/" + payload.id + "/" + token;
            
            var mailOptions = {
                from: process.env.email_id,
                to: emailAddress,
                subject: 'Reset password',
                html: `<a href=${link}>Reset password</a>`
            }
            
            transporter.sendMail(mailOptions, (err, data)=>{
                if(err) console.log(err);
                else console.log('Mail sent');
            })

            alert('Check your mail to reset password!');
            res.redirect('/');
        } 
    }
    else {
        alert('E-mail id not found');
        res.redirect('/');
    }
}

exp.passwordResetPage = async (req, res) => {
    let userID = req.params.id;
    let err, result;
    [err, result] = await to(db.query(`SELECT * FROM users WHERE user_id = ?`, [ userID ]));
    
    if(err) {
        console.log(err);
    } else {
        let secret = result[0].password;
        let payload = jwt.decode(req.params.token, secret);
        let payloadId = payload.id, token = req.params.token;
        res.render('reset-password', { payloadId, token });
    }
}

exp.resetPassword = async (req, res) => {
    
    if(req.body.password!=req.body.confirm) {
        res.sendError(null, "Passwords do not match");
    } else {
        let userId = req.body.id;
        let err, result;
        [err, result] = await to(db.query(`SELECT * FROM users WHERE user_id = ?`, [ userId ]));

        if(err) {
            
            console.log(err);
            res.sendError(null, 'Could not reset password');
        } else {
            let secret = result[0].password;
            let payload = jwt.decode(req.body.token, secret);
            let newPassword = req.body.password;
            // TODO: Hash password

            [err, result] = await to(db.query(`UPDATE users SET password = ? WHERE user_id = ?`, [ newPassword, userId ]));
            
            if(err) {
                console.log(err);
                res.sendError(null, 'Could not reset password');
            } else {
                alert('Your password has been successfully changed');
            }
        }
    }
    
    res.redirect('/');
    
}


module.exports = exp;