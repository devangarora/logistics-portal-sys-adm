const express = require('express');
var router = express.Router();
const path = require('path');
const auth = require('./auth');
const admin = require('./admin');
const category = require('./category');
const general = require('./general');
const faculty = require('./faculty');


const to = require('../utils/to');
const db = require('../config/conn');

function loggedIn(access) {
    return (req, res, next) => {
        if(typeof(req.session.isLoggedIn) === 'undefined' || req.session.isLoggedIn === false) {
            return res.redirect('/');
        } else if(req.session.type == access) { // sahi hai
            return next();
        } else {
            return res.redirect('/');
        }
    }
}

function notLoggedIn(req, res, next) {
    if (typeof(req.session.logged_in) === 'undefined' || req.session.logged_in === false) {
        return next();
    } else {
        return res.redirect('/');
    }
}

router.get('/', notLoggedIn, general.loadLogin);
router.post('/form', notLoggedIn, auth.login);
router.post('/logout', auth.logout);
router.get('/forgot', notLoggedIn, general.forgotPasswordPage);
router.post('/passwordreset', notLoggedIn, general.sendResetMail);
router.get('/resetpassword/:id/:token', notLoggedIn, general.passwordResetPage);
router.post('/resetpassword', notLoggedIn, general.resetPassword);

router.get('/category',loggedIn(1), category.displayPage);
router.post('/request', loggedIn(1), category.requestItem);
router.post('/newItemRequest', loggedIn(1), category.newItemRequest);
router.post('/category/requestHistory', loggedIn(1), category.viewHistory);

router.get('/faculty', loggedIn(2), faculty.displayPage);
router.post('/faculty/approve', loggedIn(2), faculty.approve);
router.post('/faculty/disapprove', loggedIn(2), faculty.disapprove);

router.get('/admin', loggedIn(0), admin.adminPage);
router.post('/admin/addItem', loggedIn(0), admin.addItem);
router.post('/admin/update', loggedIn(0), admin.update);

// request handling
router.post('/admin/handleRequests', loggedIn(0), admin.handleRequestPage);
router.post('/admin/approve', loggedIn(0), admin.approve);
router.get('/admin/approve', loggedIn(0), admin.handleRequestPage);
router.post('/admin/disapprove', loggedIn(0), admin.disapprove);
router.get('/admin/disapprove', loggedIn(0), admin.handleRequestPage);

// new request handling
router.get('/admin/newItemRequests', loggedIn(0), admin.newItemRequestPage);
router.post('/admin/newItemRequests', loggedIn(0), admin.newItemRequestPage);
router.post('/admin/itemAdded', loggedIn(0), admin.newItemAdded);
router.post('/admin/itemRemoved', loggedIn(0), admin.newItemRemoved);


module.exports = router;
