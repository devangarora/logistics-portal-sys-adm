const db = require('../config/conn');
const to = require('../utils/to');
const alert = require('alert-node');

let exp = {};

exp.requestItem = async (req, res) => {
    
    let id = req.body.item_id;
    let requestedQuantity = req.body.quantityreq;
    let err, result;

    let sql = 'SELECT * FROM items WHERE item_id = ?';

    [err, result] = await to(db.query(sql, [id]));

    if(err)
        res.sendError(null, 'Error');
    else {
        if(result.length <= 0) {
            res.sendError(null, 'Item not found');
        } else {
            let data = result[0];
            let availableQuantity = result[0].quantity;
             
            let user_id = req.session.cat_id; 
            let category = req.session.category;
            let itemID = data.item_id;
            let itemName = data.item_name;
            let reqdQuantity = requestedQuantity;
            let quantity = data.quantity;
            let description = data.description;
            let datetime = new Date();
            let date = datetime.toISOString().slice(0,10);
                
            let sql = `INSERT INTO requests(user_id, category, item_id, item_name, reqd_quantity, quantity, description, date_of_request, status) VALUES(?, ?, ?, ?, ?, ?, ?, ?, 'pending')`;
            let err1, result1;
            [err1, result1] = await to(db.query(sql, [ user_id, category, itemID, itemName, reqdQuantity, quantity, description, date ]));
            if(err1) {
                console.log(err1);
                alert('Request could not be sent');
            } else {
                alert('Request sent succesfully!');
            }
        }
        
    }

    res.redirect('/category');
}

// renders list of items in items table
exp.displayPage = async (req, res) => {
    let err, result;
    [err, result] = await to(db.query("SELECT * FROM items"));
    res.render('list', { items : result, name: req.session.category });
}

exp.newItemRequest = async (req, res) => {
    let itemName = req.body.name;
    let quantity = req.body.newItemQuantity;
    let description = req.body.desc; 
    let user_id = req.session.cat_id;
    let category = req.session.category;
    let datetime = new Date();
    let date = datetime.toISOString().slice(0,10);


    let sql = `INSERT INTO requested_items (user_id, category, item, quantity, description, date_of_request, status) VALUES(?, ?, ?, ?, ?, ?, 'pending')`; 
    let err, result;
    [err, result] = await to(db.query(sql, [ user_id, category, itemName, quantity, description, date]));

    if(err)
        alert('Request could not be sent');
    else
        alert('Request sent succesfully!');
    
    res.redirect('/category');
}

exp.viewHistory = async (req, res) => {
    let category = req.session.category;
    const sql = 'SELECT * FROM requests WHERE category = ?';
    let err, result;

    [err, result] = await to(db.query(sql, [ category ]));

    const sql1 = 'SELECT * FROM requested_items WHERE category = ?'
    let err1, result1;
    [err1, result1] = await to(db.query(sql1, [ category ]));
    
    if(err || err1) {
        console.log(err);
        return res.sendError(null, 'Could not load history');
    } else if(result.length === 0 && result1.length === 0) {
        res.sendError(null, 'No records found');
    } else {
        //let date = result.date_of_request.substring(0, 11);
        res.render('requestHistory', {result, result1});
    }
}

module.exports = exp;