const db = require('../config/conn');
const to = require('../utils/to');
const alert = require('alert-node');
const path = require('path');
require("dotenv").config({ path: path.join(__dirname, "../.env") });
const nodemailer = require('nodemailer')

var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    service:'gmail',
    auth:{
        user: process.env.email_id,
        pass: process.env.password
    }
});

let exp = {};

exp.adminPage = async (req, res) => {
    let err, result;
    [err, result] = await to(db.query(`SELECT * FROM items`));
    
    if(err) {
        res.sendError(null, 'Could not load admin page');
    } else {
        res.render('admin', { result });
    }
}

exp.handleRequestPage = async (req, res) => {
    let err, result;
    [err, result] = await to(db.query(`SELECT * FROM requests WHERE status = "pending" AND fac_approved = "approved"`));
    
    if(err) {
        res.sendError(null, 'Could not load request handling page');
    } else {
        res.render('handleRequests', { result });
    }
}

exp.newItemRequestPage = async (req, res) => {
    let err, result;
    [err, result] = await to(db.query(`SELECT * FROM requested_items WHERE status = "pending" AND fac_approved = "approved"`));

    if(err) {
        console.log(err);
        res.sendError(null, 'Could not load page');
    } else {
        res.render('newItemRequests', { result });
    }
}

exp.addItem = async (req, res) => {
    let name = String(req.body.item_name).trim();
    let quantity = req.body.quantity;
    let description = String(req.body.description).trim();
    let err, result;
    let sql = `INSERT INTO items(item_name, quantity, description) VALUES(?, ?, ?)`;

    [err, result] = await to(db.query(sql, [ name, quantity, description ]));
    if(err) {
        console.log(err);
        alert(`Item couldn't be added!`);
    } else {
        alert(`Item successfully added!`);
    }
    res.redirect('/admin');
}

exp.update = async (req, res) => {
    let name = req.body.item_name;
    let quantity = req.body.quantity;
    let err, result;
    let sql = `UPDATE items SET quantity = ? WHERE item_name = ?`;

    [err, result] = await to(db.query(sql, [quantity, name]));

    if(err) {
        console.log(err);
        alert('Quantity could not be updated');
    } else if(result.changedRows === 0) {
        alert('Item not found in list');
    } else {
        //console.log(result);
        alert('Quantity updated!');
    }

    res.redirect('/admin');
}

exp.approve = async (req, res) => {
    let err, result, orderID = req.body.approve;
    let sql = `SELECT * FROM requests WHERE order_id = ?`;
    [err, result] = await to(db.query(sql, [ orderID ]));
    //console.log(result);

    if(err) {
        console.log(err);
        res.sendError(null, 'Order Id not found');
    } else {
        
        let err1, result1, err2, result2, err3, result3;
        let itemID = result[0].item_id, reqdQuantity = result[0].reqd_quantity, availableQuantity = result[0].quantity;
        let approvedQuantity = req.body[req.body.approve];
        // console.log(req.body);
        // console.log(req.body[req.body.approve]); 
        let newQuantity = availableQuantity - approvedQuantity;

        if(newQuantity < 0) {
            return res.sendError(null, 'Required quantity not available in the inventory');
        } else {
            let sql1 = `UPDATE items SET quantity = ? WHERE item_id = ?`;
            [err1, result1] = await to(db.query(sql1, [ newQuantity, itemID ]));

            let sql2 = `UPDATE requests SET status = 'approved' WHERE order_id = ?`;
            [err2, result2] = await to(db.query(sql2, [ orderID ]))

            let sql3 = `UPDATE requests SET quantity = ? WHERE item_id = ?`;
            [err3, result3] = await to(db.query(sql3, [ newQuantity, itemID ]))

            if(err1 || err2 || err3) {
                return res.sendError(null, 'Request could not be approved');
            } else {
                let userID = result[0].user_id;
                let data;
                [err, data] = await to(db.query(`SELECT * FROM users WHERE user_id = ?`, [ userID ]));

                var mailOptions = {
                    from: process.env.email_id,
                    to: data[0].email,
                    subject: 'Request Accepted',
                    text: `We are glad to inform you that your item request for ${result[0].item_name} has been approved.\nRequested units: ${result[0].reqd_quantity}\nApproved units: ${approvedQuantity}`
                }
                
                transporter.sendMail(mailOptions, (err, data)=>{
                    if(err) console.log(err);
                    else console.log('Mail sent');
                })
    
            }
    
        }
    
        res.redirect('/admin/approve');
    }
}

exp.disapprove = async (req, res) => {
    let err, result, orderID = req.body.disapprove;
    let sql = `UPDATE requests SET status = 'disapproved' WHERE order_id = ?`;
    [err, result] = await to(db.query(sql, [ orderID ]));

    sql = `SELECT * FROM requests WHERE order_id = ?`;
    let error, itemData;
    [error, itemData] = await to(db.query(sql, [ orderID ]));
    
    if(err || error) {
        console.log(err);
        res.sendError(null, 'Request could not be disapproved');
    } else {

        let userID = itemData[0].user_id, err1, userData;
        [err1, userData] = await to(db.query(`SELECT * FROM users WHERE user_id = ?`, [ userID ]));

        if(err1)
            return res.sendError(null, 'Could not dissaprove');


        var mailOptions = {
            from: process.env.email_id,
            to: userData[0].email,
            subject: 'Request Declined',
            text: 'We are sorry to inform you that your item request for '+ itemData[0].item_name + '(' + itemData[0].reqd_quantity + ') has been declined.'
        }

        let data;
        transporter.sendMail(mailOptions, (err, data)=>{
            if(err) console.log(err);
            else console.log('Rejection mail sent');
        })


        res.redirect('/admin/disapprove');
    }
}

exp.newItemAdded = async (req, res) => {
    
    let err, result, reqID = req.body.added;
    [err, result] = await to(db.query(`SELECT * FROM requested_items WHERE req_id = ?`, [ reqID ]));

    if(err) {
        res.sendError(null, 'bruh bt');
    } else {

        let err1, data, userID = result[0].user_id;
        [err1, data] = await to(db.query(`SELECT * FROM users WHERE user_id = ?`, [ userID ]));

        if(err1) {
            res.sendError(null, 'kaafi bt');
        } else {
            
            let itemName = result[0].item;
        
            var mailOptions = {
                from: process.env.email_id,
                to: data[0].email,
                subject: 'Requested Item Added to Inventory',
                text: `${itemName} has been added to inventory! Please make a new request!` 
            }
            
            transporter.sendMail(mailOptions, (err, data)=>{
                if(err) console.log(err);
                else console.log('New item added mail sent');
            })
            
            let err2, result2;
            [err2, result2] = await to(db.query(`UPDATE requested_items SET status = ? WHERE req_id = ?`, [ 'added', reqID ]));
            if(err) {
                res.sendError(null, 'kill me');
            } else {
                alert('Success!');
            }
        }

    }

    res.redirect('/admin/newitemRequests');
}

exp.newItemRemoved = async (req, res) => {
    
    let err, result, reqID = req.body.removed;
    [err, result] = await to(db.query(`SELECT * FROM requested_items WHERE req_id = ?`, [ reqID ]));

    if(err) {
        res.sendError(null, 'bruh bt');
    } else {

        let err1, data, userID = result[0].user_id;
        [err1, data] = await to(db.query(`SELECT * FROM users WHERE user_id = ?`, [ userID ]));

        if(err1) {
            res.sendError(null, 'kaafi bt');
        } else {
                        
            let itemName = result[0].item;
        
            var mailOptions = {
                from: process.env.email_id,
                to: data[0].email,
                subject: 'Requested Item not added to Inventory',
                text: `${itemName} was not added to inventory!` 
            }
            
            transporter.sendMail(mailOptions, (err, data)=>{
                if(err) console.log(err);
                else console.log('New item rejected mail sent');
            })
            
            let err2, result2;
            [err2, result2] = await to(db.query(`UPDATE requested_items SET status = ? WHERE req_id = ?`, [ 'removed', reqID ]));
            if(err) {
                res.sendError(null, 'kill me');
            } else {
                alert('Success!');
            }
        }
    }

    res.redirect('/admin/newItemRequests');
}

module.exports = exp;